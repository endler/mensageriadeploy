<%-- 
    Document   : relatorio
    Created on : 14/07/2016, 15:51:40
    Author     : Mensageria
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/responsive.bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-switch.min.css" rel="stylesheet">
        <script src="js/bootstrap.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Teko:400,300' rel='stylesheet' type='text/css'>

        <script src="js/bootstrap-select.js"></script>
        <script src="js/bootstrap-switch.min.js"></script>

        <title>MensageriaWeb 1.0</title>
    </head>
    <body>
        <jsp:include page="navbar2.jsp"></jsp:include> 

            <script type="text/javascript">
                google.charts.load('current', {'packages': ['line', 'corechart', 'bar']});
                google.charts.setOnLoadCallback(drawChart);
                google.charts.setOnLoadCallback(drawChart2);

                function drawChart() {
                    var data = google.visualization.arrayToDataTable(${dadosRelatorioMensagensPorHora});
                    var options = {
                        chart: {
                            title: 'Total de mensagens por hora',
                            subtitle: ''
                        }
                    };

                    var chart = new google.charts.Line(document.getElementById('linechart_material'));

                    chart.draw(data, options);
                }

                function drawChart2() {
                    var fil = ${filtro};
                    var qtd = ${quantidade};
                    var infos = [];
                    var infosConcat = [];

                    infosConcat[0] = ['Filtro', 'Quantidade', {role: 'annotation'}];

                    for (var i = 0; i < fil.length; i++) {
                        infos[0] = fil[i];
                        infos[1] = qtd[i];
                        infos[2] = fil[i];
                        infosConcat[i + 1] = [infos[0], infos[1], infos[2]];
                    }

                    var data2 = google.visualization.arrayToDataTable(infosConcat);

                    var options2 = {
                        title: 'Ocorrências por filtro',
                        chartArea: {width: '98%'},
                        hAxis: {
                            title: 'Quantidade',
                            minValue: 0
                        },
                        vAxis: {
                            title: 'Filtro'
                        }
                    };

                    var chart2 = new google.visualization.BarChart(document.getElementById('barchart'));

                    chart2.draw(data2, options2);

                }



                $(window).resize(function () {
                    drawChart();
                    drawChart2();
                });

                $(document).ready(function () {
                    $("#barchart").hide();

                    $("#btn1").click(function () {
                        $("#barchart").fadeOut();
                        $("#linechart_material").fadeIn();
                    });
                    $("#btn2").click(function () {
                        $("#linechart_material").fadeOut();
                        $("#barchart").fadeIn(function() {
                            drawChart2();
                        });
                    });
                });




        </script>

        <div id="botoesRelatorio">
            <button class="btn btn-info" id="btn2">Ocorrências por Filtros</button>     
            <button class="btn btn-info" id="btn1">Todas as Ocorrências</button>
        </div>
        <div id="linechart_material" class="chart"></div>
        <div id="barchart" ></div>
        


        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>