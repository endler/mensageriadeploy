<nav class="navbar navbar-inverse navbar-fixed-top" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="font-family: 'Teko', sans-serif; font-size: 35px;" href="/mensageriaweb/">MensageriaWeb 1.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul id="tabs" class="nav navbar-nav" data-tabs="tabs">
                <li><a href="/mensageriaweb/#entrada">Entrada</a></li>
                <li><a href="/mensageriaweb/#filtros">Filtros</a></li>
                <li><a href="/mensageriaweb/#saida">Sa�da</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/mensageriaweb/MapaServlet"><span class="glyphicon glyphicon-map-marker white"></span></a></li>
                <li><a href="/mensageriaweb/RelatorioServlet"><span class="glyphicon glyphicon-stats white"></span></a></li>
                <li><a href="/mensageriaweb/ConfigServlet"><span class="glyphicon glyphicon-cog white"></span></a></li>
            </ul>
        </div>
        <!--        /.nav-collapse -->
    </div>
</nav>
