<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width = device-width, initial-scale = 1">

        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="css/select.dataTables.min.css" rel="stylesheet">
        <link href="css/jquery.dataTables.yadcf.css" rel="stylesheet">
        <script	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap-select.js"></script>
        <script src="js/jquery.dataTables.yadcf.js"></script>
        <script src="js/dataTables.select.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.colResize.js"></script>
        <link href="css/theme.css" rel="stylesheet" media="screen">
        <link href='https://fonts.googleapis.com/css?family=Teko:400,300' rel='stylesheet' type='text/css'>


        <title>MensageriaWeb 1.0</title>
    </head>

    <body>


        <!--- DEFAULT NAVBAR ---->
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="espacamento">
                <div class="container-fluid theme-showcase" role="main">
                    <!--- TABS --->
                    <div id="content">
                        <div id="my-tab-content" class="tab-content">
                            <div class="tab-pane active fade in" id="entrada" role="tabpanel">
                            <jsp:include page="entrada.jsp"></jsp:include>
                            </div>
                            <div class="tab-pane fade" id="filtros" role="tabpanel">
                            <jsp:include page="filtros.jsp"></jsp:include>
                            </div>
                            <div class="tab-pane fade" id="saida" role="tabpanel">
                            <jsp:include page="saida.jsp"></jsp:include>
                            </div>

                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    /* Javascript to choose between tabs */
                    $('#tabs a').click(function (e) {
                        e.preventDefault();
                        $(this).tab('show');
                    });

                    // store the currently selected tab in the hash value
                    $("ul.navbar-nav > li > a").on("shown.bs.tab", function (e) {
                        var id = $(e.target).attr("href").substr(1);
                        window.location.hash = id;
                    });

                    // on load of the page: switch to the currently selected tab
                    var hash = window.location.hash;
                    $('#tabs a[href="' + hash + '"]').tab('show');

                </script>

                <script>
                    /* Formatting function for row details on Table Entrada - Including Description */
                    function format(d) {
                        return '<table cellpadding="5" id="detail">' +
                                '<tr id="detail">' +
                                '<td id="detail">ID:</td>' +
                                '<td id="detail">' + d.id + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Título:</td>' +
                                '<td id="detail">' + d.title + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Descrição:</td>' +
                                '<td id="detail">' + d.description + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Data / Hora:</td>' +
                                '<td id="detail">' + d.timestamp + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Latitude / Longitude:</td>' +
                                '<td id="detail"><a href="http://maps.google.com/?q=' + d.latitude + ',' + d.longitude + '" target="_blank">' + d.latitude + ' / ' + d.longitude + '</a></td>' +
                                '</tr>' +
                                '</table>';
                    }
                    
                    var table;
                    $(document).ready(function () {
                        /* DataTable "Entrada" Initialization */
                        table = $('#entradaTable').DataTable({
                            "ajax": {
                                "url": "mensagemServlet",
                                "type": "POST",
                                "dataSrc": ""
                            },
                            "dom": 'Zlfrtip',
                            "autoWidth": false,
                            "colResize": {
                                "tableWidthFixed": false
                            },
                            rowId: 'id',
                            select: true,
                            "deferRender": true,
                            initComplete: function () {
//                                 $('#entradaTable_length').detach().prependTo("#divRows");
                            },
                            "columns": [
                                {
                                    "className": 'details-control',
                                    "orderable": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                {data: "id"},
                                {data: "criticity"},
                                {data: "category"},
                                {data: "title"},
                                {data: "situation"},
                                {data: "timestamp"},
                                {data: "description",
                                    "visible": false
                                }
                            ],
                            "order": [[6, 'desc']],
                            "iDisplayLength": 100
                        });

                        yadcf.init(table, [
                            {column_number: 3,
                                filter_container_id: 'selectCategoria',
                                filter_default_label: 'TODAS',
                                sort_as: 'custom',
                                style_class: 'form-control',
                                sort_as_custom_func:
                                        // Sorting Function for categorias
                                                function customFunc(a, b) {
                                                    var firstWordA = a.substr(0, a.indexOf(" "));
                                                    var firstWordB = b.substr(0, b.indexOf(" "));
                                                    var aN = parseInt(firstWordA, 10);
                                                    var bN = parseInt(firstWordB, 10);
                                                    return aN - bN;
                                                },
                                        filter_reset_button_text: false},
                        ]);

                        /* Add event listener for opening and closing details */
                        $('#entradaTable tbody').on('click', 'td.details-control', function () {
                            var tr = $(this).closest('tr');
                            var row = table.row(tr);

                            if (row.child.isShown()) {
                                // This row is already open - close it
                                row.child.hide();
                                tr.removeClass('shown');
                            } else {
                                // Open this row
                                row.child(format(row.data())).show();
                                tr.addClass('shown');
                            }
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(document).ready(function () {
                        /* DataTable "Filtros" Initialization */
                        var tableFiltros = $('#filtrosTable').DataTable({
                            "ajax": {
                                "url": "filtroServlet",
                                "type": "POST",
                                "dataSrc": ""
                            },
                            "dom": 'Zlfrtip',
                            "colResize": {
                                "tableWidthFixed": false
                            },
                            rowId: 'id',
                            select: true,
                            "deferRender": true,
                            initComplete: function () {

                            },
                            "columns": [
                                {data: "id"},
                                {data: "name"},
                                {data: "epl"},
                                {	
                                	"className": 'dt-body-center',
                                    data: null},
                                {
                                    "className": 'dt-body-center',
                                    "orderable": false,
                                    data: null,
                                    "defaultContent": '<button type="button" class="btn btn-default btn-sm editButton">' +
                                            '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>' +
                                            '</button>'
                                }
                            ],
                            "order": [[0, 'asc']],
                            "columnDefs": [
                                {
                                    "render": function (data, type, row) {
                                        var buttonType = '';
                                        var label = '';
                                        if (data.status == 'ativa') {
                                            buttonType = 'success';
                                            label = 'Ativo';
                                        } else {
                                            buttonType = 'danger';
                                            label = 'Inativo';
                                        }
                                        var text = '<button type="button" class="btn btn-' + buttonType + ' btn-sm toggleFilterButton">' + label +
                                                '</button>';
                                        return text;
                                    },
                                    "targets": 3
                                }
                            ],
                            "iDisplayLength": 25

                        });

                        // Function to show the confirmation Modal for Filter Toggling
                        $('#filtrosTable tbody').on('click', '.toggleFilterButton', function () {
                            var row = this.parentElement;
                            var data = tableFiltros.row(row).data();
                            var text = '';
                            if (data.status == 'ativa') {
                                text = '<p>Desativar Filtro "';
                            } else {
                                text = '<p>Ativar Filtro "';
                            }
                            text += '<strong>' + data.name + '</strong>"?</p>';
                            $('#textToggle').empty();
                            $(text).appendTo('#textToggle');
                            $('#hiddenIdFilter').empty().val(data.id);
                            $('#hiddenStatusFilter').empty().val(data.status);
                            $('#toggleFilterModal').modal('show');
                        });

                        // Function to show Filter Edit Modal
                        $('#filtrosTable tbody').on('click', '.editButton', function () {

                            var row = this.parentElement;
                            var data = tableFiltros.row(row).data();

                            $('#filterID').empty().val(data.id);
                            $('#filterName').empty().val(data.name);
                            $('#filterEPL').empty().val(data.epl);

                            $('#editFilter').modal('show');

                        });

                        // Function to show de confirmation for Filter Deleting
                        $('#editFilter').on('click', '#exclusionButton', function () {
                            var text = '<p>Excluir Filtro "';
                            var filterID = $('#filterID').val();
                            var filterName = $('#filterName').val();

                            text += '<strong>' + filterName + '</strong>"?</p>';
                            $('#textDelete').empty();
                            $(text).appendTo('#textDelete');

                            $('#hiddenIdDeleteFilter').empty().val(filterID);

                            $('#deleteFilterModal').modal('show');
                        });
                    });

                    /* Function to create fields dynamically on the Filter Creation Form */
                    $(function () {
                        $(document)
                                .on(
                                        'click',
                                        '.btn-add',
                                        function (e) {
                                            e.preventDefault();

                                            var controlForm = $('#btnFields').parents(
                                                    '.form-group'), currentEntry = $(this)
                                                    .parents('.entry'), newEntry = $(
                                                    currentEntry.clone()).appendTo(
                                                    controlForm);

                                            newEntry.addClass('col-sm-offset-3');
                                            newEntry.find('input').val('');
                                            controlForm
                                                    .find('.entry:not(:last) .btn-add')
                                                    .removeClass('btn-add')
                                                    .addClass('btn-remove')
                                                    .removeClass('btn-success')
                                                    .addClass('btn-danger')
                                                    .html(
                                                            '<span class="glyphicon glyphicon-minus"></span>');
                                        }).on(
                                'click',
                                '.btn-remove',
                                function (e) {
                                    $(this).parents('.entry').remove();
                                    $('#btnFields').parents('.form-group').find(
                                            '.col-sm-9:first').removeClass(
                                            'col-sm-offset-3');
                                    e.preventDefault();
                                    return false;
                                });
                    });

                </script>

                <script type="text/javascript">
                    function format2(d) {
                        return '<table cellpadding="5" id="detail">' +
                                '<tr id="detail">' +
                                '<td id="detail">ID:</td>' +
                                '<td id="detail">' + d.id + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Título:</td>' +
                                '<td id="detail">' + d.title + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Descrição:</td>' +
                                '<td id="detail">' + d.description + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Data / Hora:</td>' +
                                '<td id="detail">' + d.timestamp + '</td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Latitude / Longitude:</td>' +
                                '<td id="detail"><a href="http://maps.google.com/?q=' + d.latitude + ',' + d.longitude + '" target="_blank">' + d.latitude + ' / ' + d.longitude + '</a></td>' +
                                '</tr>' +
                                '<tr id="detail">' +
                                '<td id="detail">Filtro:</td>' +
                                '<td id="detail">' + d.filtro.name + ' / ' + d.filtro.epl + '</td>' +
                                '</tr>' +
                                '</table>';
                    }

                    $(document).ready(function () {
                        $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
                            console.log('show tab');
                            $($.fn.dataTable.tables(true)).DataTable()
                                    .columns.adjust()
                                    .responsive.recalc();
                        });

                        /* DataTable "Saída" Initialization */
                        var tableSaida = $('#saidaTable').DataTable({
                            "ajax": {
                                "url": "mensagemFiltradaServlet",
                                "type": "POST",
                                "dataSrc": ""
                            },
                            "dom": 'Zlfrtip',
                            "autoWidth": false,
                            "colResize": {
                                "tableWidthFixed": false
                            },
                            rowId: 'id',
                            select: true,
                            "deferRender": true,
                            initComplete: function () {

                            },
                            "columns": [
                                {
                                    "className": 'details-control',
                                    "orderable": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                {data: "id"},
                                {data: "filtro.name"},
                                {data: "criticity"},
                                {data: "category"},
                                {data: "title"},
                                {data: "situation"},
                                {data: "timestamp"}
                            ],
                            "order": [[7, 'desc']],
                            "iDisplayLength": 50
                        });

                        /* Reload Function for Tables "Esntrada" e "Saída" */
                        var d1 = $.get("refreshPagesServlet", function(responseJson) {
                        	$.each(responseJson, function(key, value) {
                                if (key == "refreshTimeEntrada"){
                                	setInterval(function () {
                                        table.ajax.reload();
                                    }, value);
                                }
                                else if (key == "refreshTimeSaida") {
                                	setInterval(function () {
                                        tableSaida.ajax.reload();
                                    }, value);                                	
                                }
                            });
                        });

                        yadcf.init(tableSaida, [
                            {column_number: 4,
                                filter_container_id: 'selectSaidaDiv',
                                filter_default_label: 'TODAS',
                                sort_as: 'custom',
                                style_class: 'form-control',
                                sort_as_custom_func:
                                        //Function to order the selection field by the category number ascending
                                                function customFunc(a, b) {
                                                    var firstWordA = a.substr(0, a.indexOf(" "));
                                                    var firstWordB = b.substr(0, b.indexOf(" "));
                                                    var aN = parseInt(firstWordA, 10);
                                                    var bN = parseInt(firstWordB, 10);
                                                    return aN - bN;
                                                },
                                        filter_reset_button_text: false},
                        ]);

                        /* Add event listener for opening and closing details */
                        $('#saidaTable tbody').on('click', 'td.details-control', function () {
                            var tr = $(this).closest('tr');
                            var row = tableSaida.row(tr);

                            if (row.child.isShown()) {
                                // This row is already open - close it
                                row.child.hide();
                                tr.removeClass('shown');
                            } else {
                                // Open this row
                                row.child(format2(row.data())).show();
                                tr.addClass('shown');
                            }
                        });
                    });
                </script>
            </div>

        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>