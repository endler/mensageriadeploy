<%-- 
    Document   : mapa
    Created on : 15/07/2016, 15:50:11
    Author     : Mensageria
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link href='https://fonts.googleapis.com/css?family=Teko:400,300' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/bootstrap-switch.min.css" rel="stylesheet">
        <script src="js/bootstrap-select.js"></script>
        <script src="js/bootstrap-switch.min.js"></script>

        <title>MensageriaWeb 1.0</title>

    </head>
    <body>
        <jsp:include page="navbar2.jsp"></jsp:include>
            <h1 id="margemOcorrencia">Ocorrências Filtradas</h1>

            <script>
                var map, heatmap;
                var list = ${lst};
                var desc = ${attributesDesc};
                var ver = ${verificador};
                var poi = ${poi};
                var hm = ${heatmap};
                var lastOpened = null;
                var polygon, polygonPaths;
                var markerPaths = [];
                var flagM = 0, flagP = 0, flagH = 0;

                function initMap() {
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: -22.9141308, lng: -43.445982},
                        zoom: 11
                    });

                    heatmap = new google.maps.visualization.HeatmapLayer({
                        data: getPoints(),
                        map: map,
                        dissipating: true,
                        radius: 100,
                        opacity: 0.5
                    });
                    toggleHeatmap();

                    for (var i = 0; i < list.length; i++) {
                        markerPaths[i] = addMarker(list[i], desc[i], ver[i]);
                    }
                    criaPoi();


                }

                function getPoints() {
                    var pontos = [];
                    var contaPontos = 0;
                    for (var i = 0; i < hm.length; i += 2) {
                        pontos[contaPontos] = new google.maps.LatLng(hm[i], hm[i + 1]);
                        contaPontos++;
                    }
                    return pontos;
                }

                function toggleHeatmap() {
                    heatmap.setMap(heatmap.getMap() ? null : map);
                    if (flagH == 0) {
                        document.getElementById("botaoHeatmap").innerHTML = 'Mostrar Mapa de Calor';
                        flagH = 1;
                    } else {
                        document.getElementById("botaoHeatmap").innerHTML = 'Esconder Mapa de Calor';
                        flagH = 0;
                    }

                }

                function togglePolygon() {
                    for (var i = 0; i < polygonPaths.length; i++) {
                        polygonPaths[i].setMap(polygonPaths[i].getMap() ? null : map);
                    }
                    if (flagP == 0) {
                        document.getElementById("botaoPOI").innerHTML = 'Mostrar POI';
                        flagP = 1;
                    } else {
                        document.getElementById("botaoPOI").innerHTML = 'Esconder POI';
                        flagP = 0;
                    }

                }

                function toggleMarkers() {
                    for (var i = 0; i < markerPaths.length; i++) {
                        markerPaths[i].setMap(markerPaths[i].getMap() ? null : map);
                    }
                    if (flagM == 0) {
                        document.getElementById("botaoMarker").innerHTML = 'Mostrar Marcadores';
                        flagM = 1;
                    } else {
                        document.getElementById("botaoMarker").innerHTML = 'Esconder Marcadores';
                        flagM = 0;
                    }
                }


                function addMarker(latlng, desc, verificador) {

                    if (verificador == 1) { //Waze
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                        });
                    } else {
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                        });

                    }


                    var infowindow = new google.maps.InfoWindow({
                        content: desc
                    });

                    marker.addListener('click', function () {
                        infowindow.open(map, marker);
                        if (lastOpened == null)
                            lastOpened = infowindow;
                        else {
                            lastOpened.close();
                            if (lastOpened == infowindow)
                                lastOpened = null;
                            else
                                lastOpened = infowindow;

                        }

                    });
                    return marker;
                }

                function criaPoi() {
                    polygonPaths = [];
                    for (var i = 0; i < poi.length; i++) {
                        polygon = new google.maps.Polygon({
                            paths: poi[i],
                            strokeColor: '#4d4dff',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#00004d',
                            fillOpacity: 0.35
                        });
                        polygon.setMap(map);
                        polygonPaths[i] = polygon;
                    }
                }



        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAa49NG4UVP8kc72v5gL9sNTsGsTawcYic&signed_in=true&libraries=visualization&callback=initMap">
        </script>
        <div id="map"></div>

        <div id="botoesMapa" class="container">
            <button id="botaoMarker" class="btn btn-info" onclick="toggleMarkers()">Esconder Marcadores</button>       <button id="botaoHeatmap" class="btn btn-info" onclick="toggleHeatmap()">Esconder Mapa de Calor</button>       <button id="botaoPOI" class="btn btn-info" onclick="togglePolygon()">Esconder POI</button><br>
        </div>


        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>
